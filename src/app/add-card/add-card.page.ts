import { Component, OnInit } from '@angular/core';

import { ModalController } from '@ionic/angular';
import { ToastController } from '@ionic/angular';
import { localStorageController } from '../localstorage';
import { Card } from '../card';

@Component({
  selector: 'app-add-card',
  templateUrl: './add-card.page.html',
  styleUrls: ['./add-card.page.scss'],
})
export class AddCardPage implements OnInit {
  
  public foreign: string;
  public local: string;
  public url: string;
  public description: string;

  storageController = new localStorageController()

  constructor (public viewCtrl: ModalController, public toastController: ToastController) {}

  public dismiss() {
    this.viewCtrl.dismiss();
  }

  public save() {
    if(this.foreign == null || this.foreign == "")          {this.formError(); return}
    if(this.local == null || this.local == "")              {this.formError(); return}
    if(this.description == null || this.description == "")  {this.formError(); return}
    if(this.url == null || this.url == "")
      this.url = "https://cdn.pixabay.com/photo/2020/06/05/01/28/compass-5261062_960_720.jpg"

    this.storageController.addCard(new Card(this.local, this.foreign, this.description, this.url));

    this.viewCtrl.dismiss();
  }

  private async formError() {
    const toast = await this.toastController.create({
      message: 'An error uccured, please check input',
      duration: 1000
    });

    toast.present();
  }

  ngOnInit() { }

}
