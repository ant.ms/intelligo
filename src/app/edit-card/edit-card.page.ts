import { Component, OnInit } from '@angular/core';
import { ModalController, ToastController, NavParams } from '@ionic/angular';
import { localStorageController } from '../localstorage';
import { Card } from '../card';

@Component({
  selector: 'app-edit-card',
  templateUrl: './edit-card.page.html',
  styleUrls: ['./edit-card.page.scss'],
})
export class EditCardPage implements OnInit {
  original: any;

  private id: string;

  public foreign: string;
  public local: string;
  public url: string;
  public description: string;

  storageController = new localStorageController()

  constructor (navParams: NavParams, public viewCtrl: ModalController, public toastController: ToastController) {
    this.original=navParams.get('original');
    this.id = this.original.id;
    this.foreign = this.original.wordForeign;
    this.local = this.original.wordLocal;
    this.url = this.original.imageUrl;
    this.description = this.original.description;
  }

  public dismiss() {
    this.viewCtrl.dismiss();
  }

  public save() {
    if(this.foreign == null || this.foreign == "")          {this.formError(); return}
    if(this.local == null || this.local == "")              {this.formError(); return}
    if(this.description == null || this.description == "")  {this.formError(); return}
    if(this.url == null || this.url == "")
      this.url = "https://cdn.pixabay.com/photo/2020/06/05/01/28/compass-5261062_960_720.jpg"

    this.storageController.editCard(new Card(this.local, this.foreign, this.description, this.url, this.id));

    this.viewCtrl.dismiss();
  }

  private async formError() {
    const toast = await this.toastController.create({
      message: 'An error uccured, please check input',
      duration: 1000
    });

    toast.present();
  }

  ngOnInit() { }

}
