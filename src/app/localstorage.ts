import { Card } from './card';
import { DarkMode } from './darkMode';

export class localStorageController {

    public fixLocalStorage()  {
        // create local storage if not exists
        if (localStorage.getItem('version') == null)
            this.createLocalStorage();
    }

    //If a card was answered correctly
    public somethingWasLearned(){

        //create storage item if not existing
        if(localStorage.getItem('lastlearned')==null){
            localStorage.setItem('lastlearned', JSON.stringify(new Date()).substring(1,11));
        }
        
        //create storage item if not existing
        var statistics;
        if(localStorage.getItem('statistics')==null){
            statistics = ["0", "0", "0", "0", "0", "0", "0"]
            localStorage.setItem('statistics', JSON.stringify(statistics));
        }
        
        //Set new day if new date
        if(localStorage.getItem('lastlearned')!=JSON.stringify(new Date()).substring(1,11)){
            statistics = JSON.parse(localStorage.getItem('statistics'));
            statistics[0] = statistics[1];
            statistics[1] = statistics[2];
            statistics[2] = statistics[3];
            statistics[3] = statistics[4];
            statistics[4] = statistics[5];
            statistics[5] = statistics[6];
            statistics[6] = "0";
            localStorage.setItem('lastlearned', JSON.stringify(new Date()).substring(1,11));
            localStorage.setItem('statistics', JSON.stringify(statistics));
            //Uncomment to clear correct/wrong answer chart data every day
            //localStorage.setItem('answers',JSON.stringify(["0","0"]));
        }
        //Count answers
        statistics = JSON.parse(localStorage.getItem('statistics'));
        statistics[6] = String(Number(statistics[6]) + 1);
        localStorage.setItem('statistics', JSON.stringify(statistics));
    }

    public getGraphYData(){
        if(JSON.parse(localStorage.getItem('statistics'))==null){
            return ["0","0","0","0","0","0","0"];
        }
        else{
            return JSON.parse(localStorage.getItem('statistics'));
        }
    }

    public getGraphXData(){
        //Calculate dates for the x axis
        var datetimeformat = { month: "long", day: "2-digit" }
        var dates = ["Error","Error","Error","Error","Error","Error","Error"];
        var storageentry = localStorage.getItem('lastlearned');
        var lastdate = new Date(storageentry);
        //Subtract one day per column
        var j = 0;
        for (var i = 6; i >= 0; i--){ 
            var date = new Date(lastdate.getTime()-(j*1000*60*60*24));
            dates[i] = Intl.DateTimeFormat('en-GB', datetimeformat).format(date).replace(/ /g,'. ');
            j++;
        }
        return dates;
    }

    public addAnswerData(correct){
        //Correct/Wrong answer counter
        if (JSON.parse(localStorage.getItem('answers'))==null)
            localStorage.setItem('answers',JSON.stringify(["0","0"]))

        var right = JSON.parse(localStorage.getItem("answers"))[0];
        var wrong = JSON.parse(localStorage.getItem("answers"))[1];
        
        if (correct)
            localStorage.setItem('answers',JSON.stringify([String(Number(right)+1), wrong]))
        else
            localStorage.setItem('answers',JSON.stringify([right, String(Number(wrong)+1)]))
    }

    public getAnswerAccuracy(){
        //Read correct/wrong answer data and return as persentage
        if (JSON.parse(localStorage.getItem('answers'))==null)
            return [0,0];

        var right = Number(JSON.parse(localStorage.getItem("answers"))[0]);
        var wrong = Number(JSON.parse(localStorage.getItem("answers"))[1]);  
        var perright = 100/(right+wrong)*right;
        var perwrong = 100/(right+wrong)*wrong;

        return [Number(perright.toFixed(2)), Number(perwrong.toFixed(2))];
    }

    private createLocalStorage() {
        // set localStorage version (in the future, this could be use to check for compatibility and to then call a convert function, that makes the old data compatible with the new software)
        localStorage.setItem('version', 'v1');

        //Create sample cards
        var sample = [];

        sample.push(new Card("Kompass", "Compass", "Shows the directon.", ""));
        sample.push(new Card("Baum", "Tree", "Those things are large plants.",""));
        sample.push(new Card("Welt", "World", "A planet we live on.",""));
        sample.push(new Card("Auto", "Car", "A modern way to travel.",""));
        sample.push(new Card("Wasser", "Water", "r/hydrohomies",""));
        sample.push(new Card("Flughafen", "Airport", "Planes land and takeoff here.",""));
        sample.push(new Card("Papier","Paper","This tool is used to write something.",""));
        sample.push(new Card("Haus","House","Four walls and a roof.",""));
        sample.push(new Card("Computer","Computer","A must have for every software engineer.",""));
        sample.push(new Card("Herbst","Autumn", "When the forests turn colorfull",""));
        sample.push(new Card("Frühling","Spring","Between winter and summer when the leaves turn green.",""));
        sample.push(new Card("Apfel", "Apple","Something to eat and a completely overpriced brand.",""));
        sample.push(new Card("Uhr", "Clock","Tick tock!",""));

        localStorage.setItem('cards', JSON.stringify(sample));

    }

    public addCard(card: Card) { // this function could be compacted to a single line, but for readability reasons it's not 
        // get all cards
        var entries = JSON.parse(localStorage.getItem('cards'));

        // addd card
        entries.push(card);

        // save cards again
        localStorage.setItem('cards', JSON.stringify(entries));
    }

    public editCard(card: Card) {
        // get all cards
        var entries = JSON.parse(localStorage.getItem('cards'));

        // search through every card and replace the card with the new card, once a matching (via the id) has been found
        for (let i = 0; i < entries.length; i++)
            if (entries[i].id == card.id)
                entries[i] = card;

        localStorage.setItem('cards', JSON.stringify(entries));
    }

    public removeCard(id: string) {
        // get all cards and filter the array to only contain everything with a different id than the one that needs to be removed
        var entries = JSON.parse(localStorage.getItem('cards')).filter(element => element.id !== id);

        // save changes to localStorage
        localStorage.setItem('cards', JSON.stringify(entries));
    }

    // returns all Cards in localStorage as an Array of Cards
    public getAllCardsFromLocalStorage(): Array<Card> {
        this.fixLocalStorage();
        return JSON.parse(localStorage.getItem('cards'));
    }

    // returns a random Card from the localStorage
    public getOneCardFromLocalStorage() {
        this.fixLocalStorage();
        var array = JSON.parse(localStorage.getItem('cards'));
        return array[Math.floor(Math.random() * array.length)];
    }

    public getSpecificCardFromLocalStorage(id) {
        // define output variable and set it to null, so it will return null if nothing has been found
        var correctOne: Card = null;
        this.fixLocalStorage();
        JSON.parse(localStorage.getItem('cards')).forEach(element => {
            if (element.id == id) correctOne = element; 
        });;
        return correctOne;
    }

    public changeMode(){
        if (localStorage.getItem('dark') == 'true')
            localStorage.setItem('dark','false');
        else 
            localStorage.setItem('dark','true');
        new DarkMode();
    }

    public isDark() {
        if (localStorage.getItem('dark') == 'true')
            return "dark";
        return "";
    }
}