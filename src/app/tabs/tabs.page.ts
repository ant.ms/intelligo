import { Component } from '@angular/core';
import { Tab1Page } from '../tab1/tab1.page';
import { localStorageController } from '../localstorage';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss','dark.css']
})
export class TabsPage {

  StorageController = new localStorageController

  Tab = new Tab1Page;

  constructor () {}

  changeMode(){
    this.StorageController.changeMode();
  }

  public isDark() {
    if (localStorage.getItem('dark') == 'true')
      return "dark";
    return "";
  }

}
